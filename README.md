# D3.js Playground

A place to put my little experiments with D3.js.


## Contents
**\Map\1 - base -** Draws a map of Canada.
![Map](https://gitlab.com/mmkiff/D3jsPlayground/raw/master/Map/ReadmeImages/1%20-%20base.PNG)

**\Map\2 - drawing -** A prettier map of Canada with a point plotted on it.
![Prettier Map](https://gitlab.com/mmkiff/D3jsPlayground/raw/master/Map/ReadmeImages/2%20-%20drawing.PNG)

**\Map\3 - plotting jobs -** Plots some random points on a map with simple animation.
![Prettier Map](https://gitlab.com/mmkiff/D3jsPlayground/raw/master/Map/ReadmeImages/3%20-%20plotting%20jobs.gif)

**\Map\4 - more job info -** Plots random 'projects' that are color coded by budget and filterable by client.
![Prettier Map](https://gitlab.com/mmkiff/D3jsPlayground/raw/master/Map/ReadmeImages/4%20-%20more%20job%20info.gif)

**\Map\Calgary -** A color coded map of Calgary. Colors represent the length of the name (seemed like a good idea at the time...). Mousing over an area displays the name.
![Prettier Map](https://gitlab.com/mmkiff/D3jsPlayground/raw/master/Map/ReadmeImages/calgary.gif)

**\Skd\ThemePosters -** Shows the daily theme posted in /r/sketchdaily, and colored by the amount of users who posted.
![SKD Post Data](https://gitlab.com/mmkiff/D3jsPlayground/raw/master/Skd/ReadmeImages/skd.gif)
