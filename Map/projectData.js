var PROJECT_DATA = [
  {
    "id":175398,
    "latitude":54.73657,
    "longitude":-117.58184,
    "client":1000,
    "budget":2000
  },
  {
    "id":175257,
    "latitude":54.77126,
    "longitude":-117.56564,
    "client":1000,
    "budget":5000
  },
  {
    "id":174873,
    "latitude":53.03352,
    "longitude":-115.42968,
    
    "client":1001,
    "budget":1145
  },
  {
    "id":174671,
    "latitude":50.93407,
    "longitude":-113.02758,
    
    "client":1002,
    "budget":1000
  },
  {
    "id":174656,
    "latitude":54.07063,
    "longitude":-122.36679,
    
    "client":1003,
    "budget":2000
  },
  {
    "id":174323,
    "latitude":55.78877,
    "longitude":-120.52197,
    
    "client":1003,
    "budget":16000
  },
  {
    "id":174305,
    "latitude":52.58558,
    "longitude":-114.91544,
    
    "client":1002,
    "budget":700
  },
  {
    "id":173592,
    "latitude":55.95605,
    "longitude":-120.20579,
    
    "client":1003,
    "budget":10000
  },
  {
    "id":173042,
    "latitude":54.80473,
    "longitude":-118.92526,
    
    "client":1000,
    "budget":8500
  },
  {
    "id":173041,
    "latitude":54.80473,
    "longitude":-118.92526,
    
    "client":1000,
    "budget":8500
  },
  {
    "id":172897,
    "latitude":54.82611,
    "longitude":-118.87284,
    
    "client":1000,
    "budget":5000
  },
  {
    "id":172896,
    "latitude":54.7365,
    "longitude":-118.76279,
    
    "client":1000,
    "budget":5000
  },
  {
    "id":172894,
    "latitude":54.80474,
    "longitude":-118.81769,
    
    "client":1004,
    "budget":5000
  },
  {
    "id":172840,
    "latitude":50.35014,
    "longitude":-111.34998,
    
    "client":1005,
    "budget":3500
  },
  {
    "id":172839,
    "latitude":50.34889,
    "longitude":-111.32784,
    
    "client":1005,
    "budget":1000
  },
  {
    "id":172816,
    "latitude":55.09164,
    "longitude":-119.23152,
    
    "client":1002,
    "budget":3500
  },
  {
    "id":172584,
    "latitude":52.58561,
    "longitude":-114.92012,
    
    "client":1002,
    "budget":800
  },
  {
    "id":172544,
    "latitude":56.95821,
    "longitude":-121.91705,
    
    "client":1006,
    "budget":5000
  },
  {
    "id":172394,
    "latitude":55.8794,
    "longitude":-119.29458,
    
    "client":1007,
    "budget":7000
  },
  {
    "id":172299,
    "latitude":55.09879,
    "longitude":-119.32384,
    
    "client":1002,
    "budget":8500
  },
  {
    "id":172065,
    "latitude":53.95303,
    "longitude":-118.22655,
    
    "client":1000,
    "budget":5000
  },
  {
    "id":171889,
    "latitude":52.57515,
    "longitude":-114.90391,
    
    "client":1002,
    "budget":900
  },
  {
    "id":171853,
    "latitude":55.0834,
    "longitude":-119.30605,
    
    "client":1002,
    "budget":2000
  },
  {
    "id":171490,
    "latitude":55.55606,
    "longitude":-117.05351,
    
    "client":1008,
    "budget":2000
  },
  {
    "id":171230,
    "latitude":55.11955,
    "longitude":-119.22841,
    
    "client":1002,
    "budget":3500
  }
];
